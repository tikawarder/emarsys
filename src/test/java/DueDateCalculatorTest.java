
import java.time.LocalDateTime;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import tamas.biro.emarsysdatecalculator.DueDateCalculator;
import tamas.biro.emarsysdatecalculator.InvalidDateTimeException;

public class DueDateCalculatorTest {

    private DueDateCalculator testCalculator;
    private LocalDateTime submitDate;
    private LocalDateTime dueDate;
    
    @Before
    public void setUp() {
        testCalculator = new DueDateCalculator();
    }
    
    @Test (expected = InvalidDateTimeException.class)
    public void test_givenSubmitDateIsWeekend_thenInvalidDateTimeExceptionThrows() throws InvalidDateTimeException {
        submitDate = LocalDateTime.of(2022, 04, 17, 14, 23);
        testCalculator.calculateDueDate(submitDate, 16);
    }   
    
    @Test (expected = InvalidDateTimeException.class)
    public void test_givenSubmitDateIsAfterWorkingHours_thenInvalidDateTimeExceptionThrows() throws InvalidDateTimeException {
        submitDate = LocalDateTime.of(2022, 04, 18, 17, 00);
        testCalculator.calculateDueDate(submitDate, 16);
    }
    
    @Test (expected = InvalidDateTimeException.class)
    public void test_givenSubmitDateIsBeforeWorkingHours_thenInvalidDateTimeExceptionThrows() throws InvalidDateTimeException {
        submitDate = LocalDateTime.of(2022, 04, 18, 8, 59);
        testCalculator.calculateDueDate(submitDate, 16);
    }
    
    @Test
    public void test_whenDueDateCalculationIsToWeekend_thenNextMondayIsReturned() throws Exception {
        submitDate = LocalDateTime.of(2022, 04, 20, 12, 00);
        dueDate = LocalDateTime.of(2022, 04, 25, 12, 00);
        assertEquals(dueDate, testCalculator.calculateDueDate(submitDate, 24));
    }
    
    @Test
    public void test_whenDueDateCalculationIsOverWeekend_thenPlus2DaysAreCalculated() throws Exception {
        submitDate = LocalDateTime.of(2022, 04, 20, 12, 00);
        dueDate = LocalDateTime.of(2022, 04, 25, 14, 00);
        assertEquals(dueDate, testCalculator.calculateDueDate(submitDate, 26));
    }
    
    @Test
    public void test_whenDueDateCalculationIsOver2Weekends_thenCorrectDayIsCalculated() throws Exception {
        submitDate = LocalDateTime.of(2022, 04, 20, 12, 00);
        dueDate = LocalDateTime.of(2022, 05, 04, 12, 00);
        assertEquals(dueDate, testCalculator.calculateDueDate(submitDate, 80));
    }
    
    @Test
    public void test_whenDueDateCalculationIsOnTheSameDay_thenSameDayIsCalculated() throws Exception {
        submitDate = LocalDateTime.of(2022, 04, 20, 12, 00);
        dueDate = LocalDateTime.of(2022, 04, 20, 17, 00);
        assertEquals(dueDate, testCalculator.calculateDueDate(submitDate, 5));
    }
    
    @Test
    public void test_whenDueDateCalculationIsOnTheNextDay_thenNextDayIsCalculated() throws Exception {
        submitDate = LocalDateTime.of(2022, 04, 20, 12, 00);
        dueDate = LocalDateTime.of(2022, 04, 21, 10, 00);
        assertEquals(dueDate, testCalculator.calculateDueDate(submitDate, 6));
    }
    
    @Test
    public void test_whenDueDateCalculationIsOnTheNextDayWhichIsFriday_thenNextMondayIsCalculated() throws Exception {
        submitDate = LocalDateTime.of(2022, 05, 02, 12, 00);
        dueDate = LocalDateTime.of(2022, 05, 9, 10, 00);
        assertEquals(dueDate, testCalculator.calculateDueDate(submitDate, 38));
    }
    
    @Test
    public void test_givenTurnaroundTimeIsZero_thenSameDateTimeReturned() throws Exception {
        submitDate = LocalDateTime.of(2022, 05, 02, 12, 00);
        assertEquals(submitDate, testCalculator.calculateDueDate(submitDate, 0));
    }
}
