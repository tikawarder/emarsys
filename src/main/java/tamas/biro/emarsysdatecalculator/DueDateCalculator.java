package tamas.biro.emarsysdatecalculator;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

public class DueDateCalculator {

    private static final int WORKING_DAY_START_HOUR = 9;
    private static final int WORKING_DAY_END_HOUR = 17;
    private static final int DAILY_HOURS = 24;
    private static final int WORKING_HOURS = 8;
    private static final int WEEKEND_DAYS = 2;
    private static final int WORKDAYS = 5;
    private static final int DAYS_ON_WEEK = 7;

    public LocalDateTime calculateDueDate(LocalDateTime submitDate, int turnaroundTime) throws InvalidDateTimeException {
        if (!isSubmitDateValid(submitDate)) {
            throw new InvalidDateTimeException("Invalid date!");
        }
        return checkExtraDaysForWeekend(preDaysCalculator(submitDate, turnaroundTime));
    }

    private LocalDateTime preDaysCalculator(LocalDateTime submitDate, int turnaroundTime) {
        int nbrOfDayOfWeek = submitDate.getDayOfWeek().getValue();
        int wholeDaysNeeded = turnaroundTime / WORKING_HOURS;
        int remainingHours = turnaroundTime % WORKING_HOURS;
        int totalDaysNeeded = nbrOfDayOfWeek + wholeDaysNeeded;
        int howManyWorkdaysIn = totalDaysNeeded / DAYS_ON_WEEK * WORKDAYS;
        int howManyWeekendDaysIn = totalDaysNeeded / DAYS_ON_WEEK * WEEKEND_DAYS;
        int remainingDays = totalDaysNeeded - howManyWorkdaysIn - howManyWeekendDaysIn;
        if (remainingDays > WORKDAYS) {
            howManyWeekendDaysIn++;
        }
        howManyWorkdaysIn += remainingDays;
        LocalDateTime dueDateWithWholeDays = submitDate.plusDays(howManyWorkdaysIn + howManyWeekendDaysIn * WEEKEND_DAYS - nbrOfDayOfWeek);
        return addRemainingHours(dueDateWithWholeDays, remainingHours);
    }

    private LocalDateTime checkExtraDaysForWeekend(LocalDateTime dateTime) {
        switch (dateTime.getDayOfWeek()) {
            case FRIDAY:
                return dateTime.plusDays(2);
            case SATURDAY:
                return dateTime.plusDays(2);
            case SUNDAY:
                return dateTime.plusDays(1);
            default:
                return dateTime;
        }
    }

    private LocalDateTime addRemainingHours(LocalDateTime dateTime, int hours) {
        int todaysRemainingHours = WORKING_DAY_END_HOUR - dateTime.getHour();
        if (todaysRemainingHours >= hours) {
            return dateTime.plusHours(hours);
        }
        return checkExtraDaysForWeekend(dateTime).plusHours(DAILY_HOURS - WORKING_DAY_END_HOUR + WORKING_DAY_START_HOUR + hours);
    }

    private boolean isSubmitDateValid(LocalDateTime submitDate) {
        if (submitDate.getDayOfWeek() == DayOfWeek.SATURDAY || submitDate.getDayOfWeek() == DayOfWeek.SUNDAY) {
            System.out.println("The input date is weekend. You should send working days.");
            return false;
        }
        if (submitDate.getHour() < WORKING_DAY_START_HOUR || submitDate.getHour() >= WORKING_DAY_END_HOUR) {
            System.out.println("The input date is outside of working hours. You should resend it during office time: 9AM-17PM");
            return false;
        }
        return true;
    }
}
