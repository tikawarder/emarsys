
package tamas.biro.emarsysdatecalculator;

import java.time.LocalDateTime;


public class EmarsysDateService {
    
     public static void main(String[] args) {
         
        DueDateCalculator dueDateCalculator = new DueDateCalculator();
        LocalDateTime submitDate = LocalDateTime.of(2022,04,20,16,00);
        int turnaroundTimeHours = 26;
        
         try {
             System.out.println("The calculated Due Date is " + dueDateCalculator.calculateDueDate(submitDate, turnaroundTimeHours));
         } catch (InvalidDateTimeException ex) {
             System.out.println(ex);
         }
    }
}
