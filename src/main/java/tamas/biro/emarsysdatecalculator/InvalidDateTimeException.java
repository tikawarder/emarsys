
package tamas.biro.emarsysdatecalculator;


public class InvalidDateTimeException extends Exception {

    public InvalidDateTimeException(String message) {
        super(message);
    }
    
}
